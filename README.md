### What is this repository for? ###

* "HackMicrosoft" Final Project for COE332
* Names: Yingjie Chen (yc24966), Frank Popelar (fp4433)

### Goals
* Dataset used: HistoricalQuotes.csv from data.world
* Data analysis for Microsoft stock prices from 2008 and 2018
* Grant user ability to get data and plots of said data
* Not actually used to hack Microsoft
* No real Microsoft was hurt during the process

### How do I get set up? ###

* Download this repo to your local machine or VM
* Run docker-compose up 
* Do curl http:localhost:5000/... (enter endpoints here)

### api endpoints: ###
/jobs/getresults/<string:jid>
returns job id.

/date/<string:date>
returns all data on that date (in format yyyy-mm-dd, same for all date, start, end below).

/date/start/<string:date>
returns all the days after "start" date and their data.

/date/end/<string:date>
returns all the days until this "end" date and their data.

/date/range/<string:start>/<string:end>
returns all the days between the "start" date and "end" date and their data.

/average/start/<string:date>
returns the average values of all the variables after "start" date.

/average/end/<string:date>
returns the average values of all the variables until "end" date.

/average/range/<string:start>/<string:end>
returns the average values of all the variables between "start" date and "end" date.

/average/<string:variable>/start/<string:date>
returns the average value of the "variable" after "start" date
(Variables includes: "close","volume","open","high","low").

/average/<string:variable>/end/<string:date>
returns the average value of the "variable" until "start" date

/average/<string:variable>/range/<string:start>/<string:end>
returns the average values of the "variable" between "start" date and "end" date.
*this can be used to get specific data on a specific date,
*for example: the "close" stock price for microsoft on 2018-08-30.


### Who do I talk to? ###

* Repo owner (Frank Popelar on Slack)
* John Chen on Slack