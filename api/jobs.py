#job.py
from uuid import uuid4
import redis
import json
from hotqueue import HotQueue
from time import time
import os

REDIS_IP = os.environ.get('REDIS_IP')
REDIS_PORT = os.environ.get('REDIS_PORT')

q = HotQueue("queue", host=REDIS_IP, port=REDIS_PORT, db=0)       # queue
rd = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=1)      # job log
record = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=4)  # jid to record ID database

def generate_jid():                 # generate a uuid
        return str(uuid4())

def _save_job(jid, job_dict):	    # put the job in the joblog
        rd.set(jid, json.dumps(job_dict))

def queue_job(jid):                 # put the job in the queue
        q.put(jid)

def record_job(jid, rid, tstamp):   # put a job in the record
        record.set(jid, json.dumps({rid: tstamp}))

def add_job(job_dict):              # instantiate a job
        jid = generate_jid()                    # create a jid for the job
        start = time()                          # create the start time for the job
        _save_job(jid, job_dict)                # save the job in the job log (note that the initial record id is always the jid)
        record_job(jid, jid, start)             # save the job in the jid to record ID database
        queue_job(jid)                          # queue the job
        return jid                              # return the jid given to the task

def update_jl(rid, job_dict, status):       # update the job log
        job_dict['status'] = status
        rd.set(rid, json.dumps(job_dict))
