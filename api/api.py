#api.py
from hotqueue import HotQueue
import jobs
import json
import redis
from flask import Flask, jsonify, request
import os

# The main Flask app
api = Flask(__name__)


#if __name__ == '__main__':
#	app.run(debug=True, host='0.0.0.0')

REDIS_IP = os.environ.get('REDIS_IP')
REDIS_PORT = os.environ.get('REDIS_PORT')

q = HotQueue("queue", host=REDIS_IP, port=REDIS_PORT, db=0)   # queue
rd = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=1)        # job log
data = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=2)      # database
results = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=3)   # results
record = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=4)    # record

@api.route('/date/<string:date>')
def get_date_data(date):
    date = date.replace('-', '/')
    datedata = data.get(date)
    if(datedata == None):
        return 'Invalid date'
    else:
        return datedata

@api.route('/date/start/<string:date>')
def get_date_start(date):
    date = date.replace('-', '/')
    foobar = data.get(date)
    if(foobar == None):
        return 'Invalid date'
    else:
        job_dict = {'jobtype': '1', 'status': 'in queue', 'start': date}
        jid = jobs.add_job(job_dict)
        return jid

@api.route('/date/end/<string:end>')
def get_date_end(date):
    date = date.replace('-', '/')
    foobar = data.get(date)
    if(foobar == None):
        return 'Invalid date'
    else:
        job_dict = {'jobtype': '2', 'status': 'in queue', 'end': date}
        jid = jobs.add_job(job_dict)
        return jid

@api.route('/date/range/<string:start>/<string:end>')
def get_date_range(start, end):
    start = start.replace('-', '/')
    end = end.replace('-', '/')
    foobar = data.get(start)
    boofar = data.get(end)
    if(foobar == None):
        return 'Invalid start date'
    elif(boofar == None):
        return 'Invalid end date'
    else:
        job_dict = {'jobtype': '3', 'status': 'in queue', 'start': start, 'end': end}
        jid = jobs.add_job(job_dict)
        return jid

@api.route('/average/start/<string:date>')
def get_all_averages_start(date):
    start = date.replace('-', '/')
    foobar = data.get(start)
    if(foobar == None):
        return 'Invalid date'
    else:
        job_dict = {'jobtype': '4', 'status': 'in queue', 'start': start}
        jid = jobs.add_job(job_dict)
        return jid

@api.route('/average/end/<string:date>')
def get_all_averages_end(date):
    end = date.replace('-', '/')
    foobar = data.get(end)
    if(foobar == None):
        return 'Invalid date'
    else:
        job_dict = {'jobtype': '5', 'status': 'in queue', 'end': end}
        jid = jobs.add_job(job_dict)
        return jid

@api.route('/average/range/<string:start>/<string:end>')
def get_all_averages_range(start, end):
    start = start.replace('-', '/')
    end = end.replace('-', '/')
    foobar = data.get(start)
    boofar = data.get(end)
    if(foobar == None):
        return 'Invalid start date'
    elif(boofar == None):
        return 'Invalid end date'
    else:
        job_dict = {'jobtype': '6', 'status': 'in queue', 'start': start, 'end': end}
        jid = jobs.add_job(job_dict)
        return jid

@api.route('/average/field/<string:field>/start/<string:date>')
def get_field_average_start(field, date):
    field = field.lower()
    if not(field == 'open' or field == 'close' or field == 'volume' or field == 'low' or field == 'high'):
        return 'Invalid field'
    start = date.replace('-','/')
    foobar = data.get(start)
    if(foobar == None):
        return 'Invalid date'
    else:
        job_dict = {'jobtype': '7', 'status': 'in queue', 'field': field, 'start': start}
        jid = jobs.add_job(job_dict)
        return jid

@api.route('/average/field/<string:field>/end/<string:date>')
def get_field_average_end(field, date):
    field = field.lower()
    if not(field == 'open' or field == 'close' or field == 'volume' or field -- 'low' or field == 'high'):
        return 'Invalid field'
    end = date.replace('-','/')
    foobar = data.get(end)
    if(foobar == None):
        return 'Invalid date'
    else:
        job_dict = {'jobtype': '8', 'status': 'in queue', 'field': field, 'end': end}
        jid = jobs.add_job(job_dict)
        return jid

@api.route('/average/field/<string:field>/range/<string:start>/<string:end>')
def get_field_average_range(field, start, end):
    field = field.lower()
    if not(field == 'open' or field == 'close' or field == 'volume' or field == 'low' or field == 'high'):
        return 'Invalid field'
    start = start.replace('-','/')
    end = end.replace('-','/')
    foobar = data.get(start)
    boofar = data.get(end)
    if(foobar == None):
        return 'Invalid start date'
    elif(boofar == None):
        return 'Invalid end date'
    else:
        job_dict = {'jobtype': '9', 'status': 'in queue', 'field': field, 'start': start, 'end': end}
        jid = jobs.add_job(job_dict)
        return jid

@api.route('/plot/<string:field>/start/<string:start>')
def get_plot_start(field, start):
    field = field.lower()
    if not(field == 'open' or field == 'close' or field == 'volume' or field == 'low' or field == 'high'):
        return 'Invalid field'
    start = start.replace('-','/')
    foobar = data.get(start)
    if(foobar == None):
        return 'Invalid date'
    else:
        job_dict = {'jobtype': '10', 'status': 'in queue', 'field': field, 'start': start}
        jid = jobs.add_job(job_dict)
        return jid

@api.route('/plot/<string:field>/end/<string:end>')
def get_plot_end(field, end):
    field = field.lower()
    if not(field == 'open' or field == 'close' or field == 'volume' or field == 'low' or field == 'high'):
        return 'Invalid field'
    end = end.replace('-','/')
    foobar = data.get(end)
    if(foobar == None):
        return 'Invalid date'
    else:
        job_dict = {'jobtype': '11', 'status': 'in queue', 'field': field, 'end': end}
        jid = jobs.add_job(job_dict)
        return jid

@api.route('/plot/<string:field>/range/<string:start>/<string:end>')
def get_plot_range(field, start, end):
    field = field.lower()
    if not(field == 'open' or field == 'close' or field == 'volume' or field == 'low' or field == 'high'):
        return 'Invalid date'
    start = start.replace('-','/')
    end = end.replace('-','/')
    foobar = data.get(start)
    boofar = data.get(end)
    if(foobar == None):
        return 'Invalid start date'
    elif(boofar == None):
        return 'Invalid end date'
    else:
        job_dict = {'jobtype': '12', 'status': 'in queue', 'field': field, 'start': start, 'end': end}
        jid = jobs.add_job(job_dict)
        return jid

@api.route('/jobs/getstatus/<string:jid>')
def get_job_status(jid):
    jid = str(jid)
    record_dict = json.loads(record.get(jid).decode('utf-8'))  # dictionary of all record ids associated with the given jid
    boo = 0                     # temporary variable used in comparing time stamps
    current_rid = ""            # the most recent rid
    for rid in record_dict:     # loop over all the record ids to find the most recent
        if(record_dict[rid] > boo):
            boo = record_dict[rid]
            current_rid = rid
    job_dict = json.loads(rd.get(current_rid).decode('utf-8'))
    return job_dict['status']

@api.route('/jobs/getresults/<string:jid>')
def get_job_results(jid):
    return results.get(jid)
