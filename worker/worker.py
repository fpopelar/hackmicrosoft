#worker.py
from hotqueue import HotQueue
import jobs
import time
import datetime
import redis
import os
import json
import matplotlib.pyplot as plt

REDIS_IP = os.environ.get('REDIS_IP')
REDIS_PORT = os.environ.get('REDIS_PORT')

q = HotQueue("queue", host=REDIS_IP, port=REDIS_PORT, db=0)       # queue
rd = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=1)            # job log
data = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=2)          # database
results = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=3)       # results
record = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=4)        # record

def update(jid, job_dict, status):      # update job status
    rid = jobs.generate_jid()                   # generate a new record id
    jobs.update_jl(rid, job_dict, status)       # update the job log
    jobs.record_job(jid, rid, time.time())      # update the record

def date_start(jid, startdate, jobBool):
    res = {}
    start = time.mktime(datetime.datetime.strptime(startdate, '%Y/%m/%d').timetuple())
    for d in data.keys():
        if(start <= time.mktime(datetime.datetime.strptime(d.decode('utf-8'), '%Y/%m/%d').timetuple())):
            res[d.decode('utf-8')] = json.loads(data.get(d).decode('utf-8'))
    if(jobBool):
        results.set(jid, json.dumps(res))
    return res

def date_end(jid, enddate, jobBool):
    res = {}
    end = time.mktime(datetime.datetime.strptime(enddate, '%Y/%m/%d').timetuple())
    for d in data.keys():
        if(end >= time.mktime(datetime.datetime.strptime(d.decode('utf-8'), '%Y/%m/%d').timetuple())):
            res[d.decode('utf-8')] = json.loads(data.get(d).decode('utf-8'))
    if(jobBool):
        results.set(jid, json.dumps(res))
    return res

def date_range(jid, startdate, enddate, jobBool):
    res = {}
    start = time.mktime(datetime.datetime.strptime(startdate, '%Y/%m/%d').timetuple())
    end = time.mktime(datetime.datetime.strptime(enddate, '%Y/%m/%d').timetuple())
    for d in data.keys():
        dtime = time.mktime(datetime.datetime.strptime(d.decode('utf-8'), '%Y/%m/%d').timetuple())
        if(end >= dtime and start <= dtime):
            res[d.decode('utf-8')] = json.loads(data.get(d).decode('utf-8'))
    if(jobBool):
        results.set(jid, json.dumps(res))
    return res

def field_start(jid, field, startdate, jobBool):
    res = 0
    count = 0
    start = time.mktime(datetime.datetime.strptime(startdate, '%Y/%m/%d').timetuple())
    for d in data.keys():
        dtime = time.mktime(datetime.datetime.strptime(d.decode('utf-8'), '%Y/%m/%d').timetuple())
        if(start <= dtime):
            res += json.loads(data.get(d).decode('utf-8'))[field]
            count += 1
    if(jobBool):
        results.set(jid, json.dumps(res/count))
    return res/count

def field_end(jid, field, enddate):
    res = 0
    count = 0
    end = time.mktime(datetime.datetime.strptime(enddate, '%Y/%m/%d').timetuple())
    for d in data.keys():
        dtime = time.mktime(datetime.datetime.strptime(d.decode('utf-8'), '%Y/%m/%d').timetuple())
        if (end >= dtime):
            res += json.loads(data.get(d).decode('utf-8'))[field]
            count += 1
    if(jobBool):
        results.set(jid, json.dumps(res/count))
    return res/count

def field_range(jid, field, startdate, enddate):
    res = 0
    count = 0
    start = time.mktime(datetime.datetime.strptime(startdate, '%Y/%m/%d').timetuple())
    end = time.mktime(datetime.datetime.strptime(enddate, '%Y/%m/%d').timetuple())
    for d in data.keys():
        dtime = time.mktime(datetime.datetime.strptime(d.decode('utf-8'), '%Y/%m/%d').timetuple())
        if(end >= dtime and start <= dtime):
            res += json.loads(data.get(d).decode('utf-8'))[field]
            count += 1
    if(jobBool):
        results.set(jid, json.dumps(res/count))
    return res/count

def average_start(jid, start):
    res = {'close': field_start(jid, 'close', start, False), 'volume': field_start(jid, 'volume', start, False), 'open': field_start(jid, 'open', start, False), 'high': field_start(jid, 'high', start, False), 'low': field_start(jid, 'low', start, False)}
    results.set(jid, json.dumps(res))
    return 0

def average_end(jid, end):
    res = {'close': field_end(jid, 'close', end, False), 'volume': field_end(jid, 'volume', end, False), 'open': field_end(jid, 'open', end, False), 'high': field_end(jid, 'high', end, False), 'low': field_end(jid, 'low', end, False)}
    results.set(jid, json.dumps(res))
    return 0

def average_range(jid, start, end):
    res = {'close': field_range(jid, 'close', start, end, False), 'volume': field_range(jid, 'volume', start, end, False), 'open': field_range(jid, 'open', start, end, False), 'high': field_range(jid, 'open', start, end, False), 'low': field_range(jid, 'low', start, end, False)}
    results.set(jid, json.dumps(res))
    return 0

def plot_start(jid, field, startdate):
    dt = date_start(jid, startdate, False)
    t = []
    y = []
    for key in dt:
        t.append(datetime.datetime.strptime(key, '%Y/%m/%d'))
        y.append(dt[key][field])
    plt.scatter(t,y,20,'r')
    plt.xlabel('Date')
    plt.ylabel(field)
    plt.savefig('/tmp/scatter_plot.png', dpi=150)
    file_bytes = open('/tmp/scatter_plot.png','rb').read()
    results.set(jid, file_bytes)

def plot_end(jid, field, enddate):
    dt = date_end(jid, enddate, False)
    t = []
    y = []
    for key in dt:
        t.append(datetime.datetime.strptime(key, '%Y/%m/%d'))
        y.append(dt[key][field])
    plt.scatter(t,y,20,'r')
    plt.xlabel('Date')
    plt.ylabel(field)
    plt.savefig('/tmp/scatter_plot.png',dpi=150)
    file_bytes = open('/tmp/scatter_plot.png','rb').read()
    results.set(jid, file_bytes)

def plot_range(jid, field, start, end):
    dt = date_range(jid, startdate, enddate, False)
    t = []
    y = []
    for key in dt:
        t.append(datetime.datetime.strptime(key, '%Y/%m/%d'))
        y.append(dt[key][field])
    plt.scatter(t,y,20,'r')
    plt.xlabel('Date')
    plt.ylabel(field)
    plt.savefig('/tmp/scatter_plot.png',dpi=150)
    file_bytes = open('/tmp/scatter_plot.png','rb').read()
    results.set(jid, file_bytes)

@q.worker
def execute_job(jid):
    job_dict = json.loads(rd.get(jid).decode('utf-8'))                      # get the job information from the job log
    update(jid, job_dict, "in progress")
    if(int(job_dict['jobtype']) == 0):          # test job
        testing(jid, job_dict)

    elif(int(job_dict['jobtype']) == 1):        # start date
        date_start(jid, job_dict['start'], True)
        update(jid, job_dict, 'completed')

    elif(int(job_dict['jobtype']) == 2):        # end date
        date_end(jid, job_dict['end'], True)
        update(jid, job_dict, 'completed')
    
    elif(int(job_dict['jobtype']) == 3):        # date range
        date_range(jid, job_dict['start'], job_dict['end'], True)
        update(jid, job_dict, 'completed')

    elif(int(job_dict['jobtype']) == 4):        # average start
        average_start(jid, job_dict['start'])
        update(jid, job_dict, 'completed')

    elif(int(job_dict['jobtype']) == 5):        # average end
        average_end(jid, job_dict['end'])
        update(jid, job_dict, 'completed')

    elif(int(job_dict['jobtype']) == 6):        # average range
        average_range(jid, job_dict['start'], job_dict['end'])
        update(jid, job_dict, 'completed')

    elif(int(job_dict['jobtype']) == 7):        # field average start
        field_start(jid, job_dict['field'], job_dict['start'], True)
        update(jid, job_dict, 'completed')
    
    elif(int(job_dict['jobtype']) == 8):        # field average end
        field_end(jid, job_dict['field'], job_dict['end'], True)
        update(jid, job_dict, 'completed')

    elif(int(job_dict['jobtype']) == 9):        # field average range
        field_range(jid, job_dict['field'], job_dict['start'], job_dict['end'], True)
        update(jid, job_dict, 'completed')
    
    elif(int(job_dict['jobtype']) == 10):       # plot start
        plot_start(jid, job_dict['field'], job_dict['start'])
        update(jid, job_dict, 'completed')

    elif(int(job_dict['jobtype']) == 11):       # plot end
        plot_end(jid, job_dict['field'], job_dict['end'])
        update(jid, job_dict, 'completed')

    elif(int(job_dict['jobtype']) == 12):       # plot range
        plot_range(jid, job_dict['field'], job_dict['start'], job_dict['end'])
        update(jib, job_dict, 'completed')

    else:
        update(jid, job_dict, "error: no such job type")

# populate the database
with open("HistoricalQuotes.txt", 'r') as f:
    for line in f:
        line = line.replace("\"", "")
        line = line[:-2]
        boo = line.split(',')
        data.set(str(boo[0]), json.dumps({'close': float(boo[1]), 'volume': float(boo[2]), 'open': float(boo[3]), 'high': float(boo[4]), 'low': float(boo[5])}))

execute_job()
